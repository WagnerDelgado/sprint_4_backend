package com.utpl.edu.ec.model;

import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="cita")
public class Cita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdCita;
	
	@Column(name = "fecha_cita", nullable = false)
	private LocalDateTime fecha_cita;
	
	@Column(name = "hora_cita", nullable = false, unique = true)
	private LocalTime hora_cita;
	
	@ManyToOne
	@JoinColumn(name = "IdPaciente", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_paciente"))
	private Paciente paciente;
	
	@ManyToOne
	@JoinColumn(name = "IdMedico", nullable = false, foreignKey = @ForeignKey(name = "FK_cita_medico"))
	private Medico medico;
	
	

}
