package com.utpl.edu.ec.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.utpl.edu.ec.dto.EspecialidadDTO;
import com.utpl.edu.ec.dto.MedicoDTO;
import com.utpl.edu.ec.dto.PacienteDTO;
import com.utpl.edu.ec.exception.ModeloNotFoundException;
import com.utpl.edu.ec.model.Especialidad;
import com.utpl.edu.ec.model.Medico;
import com.utpl.edu.ec.model.Paciente;
import com.utpl.edu.ec.services.IMedicoService;
import com.utpl.edu.ec.services.IPacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {
	
	@Autowired
	private IPacienteService service;
	
	@Autowired
	private ModelMapper mapper;

	@GetMapping
	public ResponseEntity<List<PacienteDTO>> listar() throws Exception{
		List<PacienteDTO> lista = service.listar().stream().map(p -> mapper.map(p, PacienteDTO.class)).collect(Collectors.toList());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	//@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<PacienteDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
		PacienteDTO dtoResponse;
		Paciente obj = service.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}else {
			dtoResponse = mapper.map(obj, PacienteDTO.class);
		}
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
	}
	
	/*@PostMapping
	public ResponseEntity<EspecialidadDTO> registrar(@Valid @RequestBody EspecialidadDTO dtoRequest) throws Exception {
		Especialidad p = mapper.map(dtoRequest, Especialidad.class);
		Especialidad obj = service.registrar(p);
		EspecialidadDTO dtoResponse = mapper.map(obj, EspecialidadDTO.class);
		return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED);
	}*/
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody PacienteDTO dtoRequest) throws Exception {
		Paciente p = mapper.map(dtoRequest, Paciente.class);
		Paciente obj = service.registrar(p);
		PacienteDTO dtoResponse = mapper.map(obj, PacienteDTO.class);
		//localhost:8080/Especialidads/9
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dtoResponse.getIdPaciente()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<PacienteDTO> modificar(@Valid @RequestBody PacienteDTO dtoRequest) throws Exception {
		Paciente pac = service.listarPorId(dtoRequest.getIdPaciente());
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdPaciente());	
		}		
		
		Paciente p = mapper.map(dtoRequest, Paciente.class);		 
		Paciente obj = service.modificar(p);		
		PacienteDTO dtoResponse = mapper.map(obj, PacienteDTO.class);
		
		return new ResponseEntity<>(dtoResponse, HttpStatus.OK);		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Paciente pac = service.listarPorId(id);
		
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
