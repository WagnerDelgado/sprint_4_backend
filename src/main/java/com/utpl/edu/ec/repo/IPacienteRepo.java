package com.utpl.edu.ec.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.utpl.edu.ec.model.Paciente;

public interface IPacienteRepo extends IGenericRepo<Paciente, Integer> {

}
