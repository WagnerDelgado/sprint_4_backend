package com.utpl.edu.ec.repo;

import com.utpl.edu.ec.model.Especialidad;

public interface IEspecialidadRepo extends IGenericRepo<Especialidad, Integer>{

}
