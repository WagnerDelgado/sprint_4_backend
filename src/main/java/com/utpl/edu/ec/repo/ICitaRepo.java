package com.utpl.edu.ec.repo;

import com.utpl.edu.ec.model.Cita;

public interface ICitaRepo extends IGenericRepo<Cita, Integer>{

}
