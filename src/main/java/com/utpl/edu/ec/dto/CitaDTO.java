package com.utpl.edu.ec.dto;

import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.validation.constraints.NotNull;

import com.utpl.edu.ec.model.Medico;
import com.utpl.edu.ec.model.Paciente;

public class CitaDTO {
	
	private Integer IdCita;
	@NotNull
	private LocalDateTime fecha_cita;
	@NotNull
	private LocalTime hora_cita;
	@NotNull
	private Paciente paciente;
	@NotNull
	private Medico medico;
	public Integer getIdCita() {
		return IdCita;
	}
	public void setIdCita(Integer idCita) {
		IdCita = idCita;
	}
	public LocalDateTime getFecha_cita() {
		return fecha_cita;
	}
	public void setFecha_cita(LocalDateTime fecha_cita) {
		this.fecha_cita = fecha_cita;
	}
	public LocalTime getHora_cita() {
		return hora_cita;
	}
	public void setHora_cita(LocalTime hora_cita) {
		this.hora_cita = hora_cita;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
}
