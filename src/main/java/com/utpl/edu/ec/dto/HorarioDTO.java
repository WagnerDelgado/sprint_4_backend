package com.utpl.edu.ec.dto;

import javax.validation.constraints.NotNull;

public class HorarioDTO {
	
	private Integer idHorario;
	@NotNull
	private String dias;
	@NotNull
	private String hora_inicio;
	@NotNull
	private String hora_fin;
	
	public Integer getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}
	public String getDias() {
		return dias;
	}
	public void setDias(String dias) {
		this.dias = dias;
	}
	public String getHora_inicio() {
		return hora_inicio;
	}
	public void setHora_inicio(String hora_inicio) {
		this.hora_inicio = hora_inicio;
	}
	public String getHora_fin() {
		return hora_fin;
	}
	public void setHora_fin(String hora_fin) {
		this.hora_fin = hora_fin;
	}
}
