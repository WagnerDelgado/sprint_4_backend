package com.utpl.edu.ec.dto;

import javax.validation.constraints.NotNull;

public class PacienteDTO {

	private Integer IdPaciente;
	@NotNull
	private String cedula;
	@NotNull
	private String nombres;
	@NotNull
	private String apellidos;
	@NotNull
	private String sexo;
	private String telefono;
	@NotNull
	private String usuario;
	@NotNull
	private String password;
	public Integer getIdPaciente() {
		return IdPaciente;
	}
	public void setIdPaciente(Integer idPaciente) {
		IdPaciente = idPaciente;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
