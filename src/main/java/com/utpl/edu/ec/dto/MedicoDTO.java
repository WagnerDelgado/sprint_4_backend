package com.utpl.edu.ec.dto;

import javax.validation.constraints.NotNull;

public class MedicoDTO {
	
	private Integer IdMedico;
	@NotNull
	private String cedula;
	@NotNull
	private String nombres;
	@NotNull
	private String apellidos;
	@NotNull
	private EspecialidadDTO especialidad;
	@NotNull
	private HorarioDTO horario;
	
	public Integer getIdMedico() {
		return IdMedico;
	}
	public void setIdMedico(Integer idMedico) {
		IdMedico = idMedico;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public EspecialidadDTO getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(EspecialidadDTO especialidad) {
		this.especialidad = especialidad;
	}
	public HorarioDTO getHorario() {
		return horario;
	}
	public void setHorario(HorarioDTO horario) {
		this.horario = horario;
	}

}
