package com.utpl.edu.ec.service.Impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.utpl.edu.ec.model.Medico;
import com.utpl.edu.ec.repo.IGenericRepo;
import com.utpl.edu.ec.repo.IMedicoRepo;

import com.utpl.edu.ec.services.IMedicoService;

@Service
public class MedicoServiceImpl extends CRUDImpl<Medico, Integer> implements IMedicoService{
	
	@Autowired
	private IMedicoRepo repo;

	@Override
	protected IGenericRepo<Medico, Integer> getRepo() {
		return repo;
	}
}
