package com.utpl.edu.ec.services;

import com.utpl.edu.ec.model.Cita;
import com.utpl.edu.ec.model.Especialidad;

public interface ICitaService extends ICRUD<Cita, Integer> {

}
